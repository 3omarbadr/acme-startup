$(function () {

    var currentColumns = [];
    // currentColumns.push({
    //     data: 'record_select',
    //     name: 'record_select',
    //     searchable: false,
    //     sortable: false,
    //     width: '1%'
    // });

    var thLen = $('.data-table thead th').length;
    $('.data-table thead th').each(function (index) {
        $(this).addClass('text-center align-middle');
        if (index < thLen - 1) {
            var data = $(this).attr('dt-name');
            var col_type = $(this).attr('dt-type');
            if (col_type == 'text') {
                $('#searchable-row').append('<th class="text-center w-25"><input type="text" class="text-center filter-table" style="padding: 5px"/></th>');
            }
            if (col_type == 'date') {
                $('#searchable-row').append(`<th class="text-center w-25"><input type="text" class="text-center filter-table date-range" style="padding: 5px"/></th>`);
            }

            if (col_type == 'select') {
                var select_options = $(this).attr('dt-options');
                if ($(this).attr('dt-enc') == "yes") {
                    var select_options = atob($(this).attr('dt-options'));
                }
                select_options = select_options.replace(/\'/g, '\"');
                let options = JSON.parse(select_options);
                let th_select = '<th class="text-center"><select dir="' + lang + '" type="text" class="select-custom text-center filter-table">';
                th_select += '<option value="">'+ select_all +'</option>'
                $.each(options, function (index, value) {
                    th_select += '<option value="' + index + '">';
                    th_select += value;
                    th_select += '</option>';
                });

                th_select += '</select></th>';
                $('#searchable-row').append(th_select);
                $('.select-custom').select2();
            }
            if (col_type == 'forignkey') {
                var select_options = $(this).attr('dt-options');
                if ($(this).attr('dt-enc') == "yes") {
                    var select_options = atob($(this).attr('dt-options'));
                }
                select_options = select_options.replace(/\'/g, '\"');
                let options = JSON.parse(select_options);
                let th_select = '<th class="text-center"><select dir="' + lang + '" type="text" class="select-custom text-center filter-table">';
                th_select += '<option value="">'+ select_all +'</option>'
                $.each(options, function (index, value) {
                    th_select += '<option value="' + index + '">';
                    th_select += value;
                    th_select += '</option>';
                });

                th_select += '</select></th>';
                $('#searchable-row').append(th_select);
                $('.select-custom').select2();
            }
            if (col_type == 'image') {
                $('#searchable-row').append('<th class="align-middle text-center"></th>');
                currentColumns.push({
                    data: data.toLowerCase(),
                    name: data.toLowerCase(),
                    searchable: false,
                    sortable: false

                });

            } else {
                currentColumns.push({
                    data: data.toLowerCase(),
                    name: data.toLowerCase(),
                    sortable: false,
                });

            }
        } else {
            $('#searchable-row').append('<th class="align-middle text-center"></th>');
        }
    });

    currentColumns.push({
        data: 'actions',
        name: 'actions',
        searchable: false,
        sortable: false
    });
    
    $(".data-table").dataTable().fnDestroy();
    var table = $('.data-table').DataTable({
        dom: "ltipr",
        serverSide: true,
        processing: false,
        language: {
            "url": dataTablesLanguageLink,
        },
        ajax: {
            "url": dataTablesSearchLink,
            "type": "GET",
        },
        columns: currentColumns,
        order: [],
        createdRow: function (row, data, dataIndex, cells) {
            $(cells).addClass('text-center align-middle');
        },
        drawCallback: function (settings) {
            $('.record__select').prop('checked', false);
            $('#record__select-all').prop('checked', false);
            $('#record-ids').val();
            $('#bulk-delete').attr('disabled', true);
        }
    });//end datatable

    table.columns().eq(0).each(function (colIdx) {
        $('.filter-table', table.column(colIdx).header()).on('keyup change apply.daterangepicker cancel.daterangepicker', function (ev, picker) {
            if ($(this).hasClass('date-range')) {
                if (ev.type == 'apply') {
                    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                } else {
                    $(this).val('');
                }
            }
            table
                .column(colIdx)
                .search(this.value)
                .draw();
        });
    });

    /**
     * bulk-delete js
     */
    $("#record__select-all").on('click', function () {
        var isChecked = $(this).prop('checked');
        $("input[type=checkbox].record__select").prop('checked', isChecked);
    });

    $(document).on("change", "input[type=checkbox]#record__select-all", function () {
        var isChecked = $(this).prop('checked');
        if (isChecked) {
            var checkedInputs = [];
            $('input[type=checkbox].record__select').filter(":checked").each(function () {
                checkedInputs.push(this.value);
            });
            $('#record-ids').val(checkedInputs);
            $('#bulk-delete').removeAttr('disabled');
        } else {
            $('#bulk-delete').attr('disabled', true);
        }
    });

    $(document).on("change", "input[type=checkbox].record__select", function () {
        var count = $('input[type=checkbox].record__select').filter(":checked").length;
        if (count > 1) {
            $("#record__select-all").prop('checked', true).trigger('change');
        } else {
            $("#record__select-all").prop('checked', false).trigger('change');
        }
    });

    $('input.date-range').daterangepicker({
        opens: 'center',
        autoUpdateInput: false,
        linkedCalendars: false,
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": apply,
            "cancelLabel": cancel,
        }
    });

});
